from django.contrib.auth.models import User
from django.db import models

from shop.models import Product


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    total = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def get_total_items(self):
        total = 0
        return total

    def __str__(self):
        return str(self.id)

    def total_value_cart(self):
        result = sum(items.product.price * items.quantity for items in self.item_set.all())
        self.total = result


class Item(models.Model):

    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, null=True)
    quantity = models.PositiveIntegerField()

    # not allowing duplicates of the same Item in the same Cart
    class Meta:
        unique_together = ('product', 'cart')

    def __str__(self):
        return self.product.name

    def get_total_price(self):
        return self.product.price * self.quantity
