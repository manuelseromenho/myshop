#global imports
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic import DeleteView


#local imports
from shop.models import Product
from .models import Cart, Item


# class ItemDelete(DeleteView):
#     model = Item
#     success_url = '/cart'
    #pk_url_kwarg = "id"

def delete_item(request, pk):
    try:

        item = Item.objects.get(id=pk)
        item.delete()

        user = request.user
        cart = Cart.objects.filter(user=user).last()
        items = Item.objects.filter(cart=cart)
        total = sum(i.product.price * i.quantity for i in items)
        cart.total = total
        cart.save()

    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse("cart:show_cart"))
    return HttpResponseRedirect(reverse("cart:show_cart"))


def delete_all_items(request):
    try:

        item = Item.objects.all()
        item.delete()

        user = request.user
        cart = Cart.objects.filter(user=user).last()
        items = Item.objects.filter(cart=cart)
        total = sum(i.product.price * i.quantity for i in items)
        cart.total = total
        cart.save()

    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse("cart:show_cart"))
    return HttpResponseRedirect(reverse("cart:show_cart"))


def add_cart(request):
    slug = request.POST.get('slug', '')
    quantity = request.POST.get("quantity", "")

    if request.method == 'POST':
        product = get_object_or_404(Product, slug=slug)

        if quantity == '':
            return render(request, 'shop/product/details.html', {'product': product, 'stock_error': 1})
        elif int(quantity) < 1 or product.stock < int(quantity):
            return render(request, 'shop/product/details.html', {'product': product, 'stock_error': 1})

        # Pesquisar por user
        user = request.user

        # ***** Filter cart by user, if it doesnt exist create
        if Cart.objects.filter(user=user):
            cart = Cart.objects.filter(user=user).last()
        else:
            cart = Cart(user = user)
            cart.save()


        # Com o cart verificar se existem o item dentro do cart
        # se existir aumenta a quantity
        # cc Cria o item e adiciona ao cart
        item_repeated = Item.objects.filter(cart=cart, product=product).first()
        if not item_repeated:
            # Criar novo Item
            item = Item(
                product=product,
                quantity=quantity,
                cart=cart
            )
            item.save()
        else:
            # Update Item
            item_repeated.quantity += int(quantity)
            item_repeated.save()
            # return render(request, 'cart/cart.html')

        items = Item.objects.filter(cart=cart)
        total = sum(i.product.price * i.quantity for i in items)
        cart.total = total
        cart.save()

        return show_cart(request)

        # return render(request, 'cart/cart.html', {'product': product})


def show_cart(request):

    user = request.user

    # ***** Filter cart by user, if it doesnt exist create
    if Cart.objects.filter(user=user):
        cart = Cart.objects.filter(user=user).last()
    else:
        cart = Cart(user=user)
        cart.save()

    #items = Item.objects.filter(cart = cart).select_related('product')
    items = Item.objects.filter(cart=cart)

    # total = sum(i.product.price*i.quantity for i in items)
    total = cart.total

    #item = Item.objects.filter(item=id_produto).first()

    return render(request, 'cart/cart.html', {'items': items, 'total': total, })


def update_cart(request):
    slug = request.POST.get('slug', '')
    quantity = request.POST.get("quantity", "")
    # slug = request.GET.get('slug')
    # quantity = request.GET.get("quantity")
    stock_error = 0
    items = None
    total = 0

    if request.method == 'POST':
        product = get_object_or_404(Product, slug=slug)

        # Pesquisar por user
        user = request.user

        # ***** Filter cart by user, if it doesnt exist create
        if Cart.objects.filter(user=user):
            cart = Cart.objects.filter(user=user).last()
        else:
            cart = Cart(user= user)
            cart.save()

        items = Item.objects.filter(cart=cart)
        item_repeated = Item.objects.filter(cart=cart, product=product).first()

        # Update Item
        if product.stock >= int(quantity) and int(quantity)>0:
            item_repeated.quantity = int(quantity)
            item_repeated.save()

            total = sum(i.product.price * i.quantity for i in items)
            cart.total = total
            cart.save()

            stock_error = 0

            # answer = ""
            # data = {
            #     'respond': answer,
            #     'quantity': quantity,
            # }
            # return JsonResponse(data)
        else:
            stock_error = 1
            total = cart.total
            # answer = "Valor Superior ao Stock Existente"
            # data = {
            #     'respond': answer,
            #     'quantity': quantity,
            # }
            # return JsonResponse(data)

    return render(request, 'cart/cart.html', {'items': items, 'total': total, 'stock_error': stock_error})
