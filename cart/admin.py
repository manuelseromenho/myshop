from django.contrib import admin
from .models import Cart, Item


class CartAdmin(admin.ModelAdmin):
    list_display = ["user", "total", "created", "updated"]
    list_filter = ["created"]
    list_editable = ["user"]


class ItemAdmin(admin.ModelAdmin):
    list_display = ["product", "cart", "quantity"]
    list_filter = ["cart"]
    list_editable = ["product", "cart", "quantity"]


admin.site.register(Cart, CartAdmin)
admin.site.register(Item, ItemAdmin)
