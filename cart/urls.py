from django.conf.urls import url
from django.contrib import admin
from . import views


urlpatterns = [

    # url(r'^adicionar/(?P<product_slug>[-\w]+)/$', views.adicionar_carrinho, name="adicionar_carrinho"),
    url(r'^update/$', views.update_cart, name="update_cart"),
    url(r'^add/$', views.add_cart, name="add_cart"),
    url(r'^$', views.show_cart, name="show_cart"),
    #url(r'^cart/delete/(?P<pk>\d+)/$', views.ItemDelete.as_view(), name='cart_item_delete'),
    url(r'^cart/deleteall/$', views.delete_all_items, name='cart_delete_allitems'),
    url(r'^cart/delete/(?P<pk>\d+)/$', views.delete_item, name='cart_item_delete'),


]
