from django.contrib.auth.decorators import login_required
from django.contrib import auth

from cart.models import Cart, Item


def total_on_base(request):

    user = request.user

    if user.is_authenticated():
        if not Cart.objects.filter(user=user):
            total_on_base = 0
        else:
            cart = Cart.objects.filter(user=user).last()
            total_on_base = cart.total
    else:
        total_on_base = 0

    return {'total_on_base': total_on_base}
