#django imports
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.generic import DeleteView
from django.contrib.auth.models import User

#local imports
from orders.models import Order, OrderItem
from cart.models import Cart, Item
from .models import Category, SubCategory, Product, Profile, PaymentMethod
from .forms import LoginForm, UserRegistrationForm, UserEditForm, ProfileEditForm, UserDetailsForm, ProfileDetailsForm


def product_list(request, sub_category_slug=None, category_slug=None):
    category = None
    categories = Category.objects.all()

    subcategory = None
    subcategories = SubCategory.objects.all()

    products = Product.objects.filter(available=True)

    min_price = request.GET.get('min_price', 0)
    max_price = request.GET.get('max_price', 10000)

    if not min_price:
        min_price = 0
    if not max_price:
        max_price = 3000

    products = products.filter(price__gte=min_price, price__lte=max_price)

    # if category_slug:
    #     category = get_object_or_404(Category, slug=category_slug)
    #     produtos = produtos.filter(category=category)

    if sub_category_slug:
        subcategory = get_object_or_404(SubCategory, slug=sub_category_slug)
        products = products.filter(subcategory=subcategory)

    return render(
        request,
        'shop/product/list.html',
        {
            'subcategory': subcategory,
            'subcategories': subcategories,
            'products': products
        }
    )


def product_details(request, product_slug):

    # product = Product.objects.filter(slug=product_slug).first()
    # product = Product.objects.get(slug=product_slug)

    #if request.method == 'GET':
    product = get_object_or_404(Product, slug=product_slug)

    return render(request, 'shop/product/details.html', {'product': product})


@login_required
def dashboard(request):
    profile_photo = None

    # I'm almost certain that this check becomes redundant with the above decorator
    if request.user.is_authenticated:
        try:
            profile = Profile.objects.get(user=request.user)
            profile_photo = profile.photo
        except ObjectDoesNotExist:
            pass

        user_form = UserDetailsForm(instance=request.user)

        profile_form = ProfileDetailsForm(instance=request.user.profile)

    return render(request, 'shop/dashboard.html',
                  {'section': 'dashboard',
                   'photo': profile_photo,
                   'user_form': user_form,
                   'profile_form': profile_form,})


def register(request):
    if request.method == 'POST':

        user_form = UserRegistrationForm(request.POST)

        if user_form.is_valid():
            # Create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # Set the chosen password
            new_user.set_password(
                user_form.cleaned_data['password'])
            # Save the User object
            new_user.save()
            # Create the user profiles
            profile = Profile.objects.create(user=new_user)
            return render(request,
                          'register_done.html',
                          {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request,
                  'register.html',
                  {'user_form': user_form})


@login_required
def edit(request):

    if request.method == 'POST':

        payment_methods = PaymentMethod.objects.all()

        user_form = UserEditForm(
            instance=request.user,
            data=request.POST)
        profile_form = ProfileEditForm(
            instance=request.user.profile,
            data=request.POST,
            files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            # user = User.objects.get(username=user_form.cleaned_data["username"])
            # user.set_password(user_form.cleaned_data["password"])
            user_form.save()
            profile_form.save()
    else:
        payment_methods = PaymentMethod.objects.all()
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)

    return render(
        request,
        'shop/edit.html',
        {
            'user_form': user_form,
            'profile_form': profile_form,
            'payment_methods': payment_methods,
        }
    )


@login_required
def order_list(request):

    profile = Profile.objects.get(user=request.user)

    user_nif = profile.nif

    orders = Order.objects.filter(nif=user_nif)

    return render(request, 'shop/order_list.html', {'orders': orders, })

