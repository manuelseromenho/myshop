from django.contrib import admin
from .models import Category, SubCategory, Product, Profile, PaymentMethod


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug':('name',)}


class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ['category', 'subcategory', 'slug']
    prepopulated_fields = {'slug':('name',)}

    # Allows to customize the field "name" in admin and still maintain the order by "name"
    def subcategory(self, obj):
        return "{}".format(obj.name)
    subcategory.short_description = "SubCategory"
    subcategory.admin_order_field = "name"


class ProductAdmin(admin.ModelAdmin):
    list_display = ["name", "slug", "price", "stock", "available", "created", "updated"]
    list_filter = ["available", "created", "updated"]
    list_editable = ['price', 'stock', 'available']
    prepopulated_fields = {'slug': ('name',)}


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'birthday', 'photo']


class PaymentMethodAdmin(admin.ModelAdmin):
    list_display = ['payment_method',  'method_tax']
    list_editable = ['payment_method',  'method_tax']


admin.site.register(SubCategory, SubCategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(PaymentMethod, PaymentMethodAdmin)
