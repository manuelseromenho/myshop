from django.contrib.auth.models import User
from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _


class Category(models.Model):

    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category', args=[self.slug])


class SubCategory(models.Model):

    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True, null = True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'subcategory'
        verbose_name_plural = 'subcategories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product_list_by_subcategory', args=[self.slug])


class Product(models.Model):

    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True)
    image = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)
    description = models.TextField(max_length=300, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    stock = models.PositiveIntegerField()
    available = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug'),)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product_details', args=[self.slug])


class PaymentMethod(models.Model):
    payment_method = models.TextField(max_length=20, blank=True)
    method_tax = models.DecimalField(max_digits=3, decimal_places=2)

    def __str__(self):
        return '{}'.format(self.payment_method)


class Profile(models.Model):
    payment_method = models.ForeignKey(PaymentMethod, default=1, on_delete=models.CASCADE)
    user = models.OneToOneField(User)
    email = models.EmailField(_('email address'), blank=True, unique=True)
    post_address = models.TextField(max_length=100, blank=True)
    billing_address = models.TextField(max_length=100, blank=True)
    nif = models.CharField(max_length=11, blank=True)
    birthday = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to='utilizadores/%Y/%m/%d', blank=True)

    def __str__(self):
        return 'User Profile of {}'.format(self.user.username)






