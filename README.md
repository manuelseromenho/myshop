# djangoSimpleCatalog_v1

Requirements: 

Python 3.6
Django 1.8.11
others (check requirements file)


Create an .env file inside "myshop" folder (for the credentials of Mailgun and Stripe)
# Mailgun  
EMAIL_HOST = 'smtp.mailgun.org'  
EMAIL_PORT = 587  
Fill with the credentials from Mailgun dashboard  
EMAIL_HOST_USER = ''  
EMAIL_HOST_PASSWORD = ''  

# Stripe  
Fill with the keys from Stripe dashboard  
STRIPE_PUBLIC_KEY = 'pk_test_'  
STRIPE_SECRET_KEY = 'sk_test_'  
