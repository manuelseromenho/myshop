#django imports
from django.shortcuts import render
from django.core.mail import send_mail
import stripe

#local imports
from .models import OrderItem, Order
from .forms import OrderCreateForm, UserCreateForm,ProfileCreateForm
from cart.models import Cart, Item
from shop.models import Profile
from myshop import settings


def order_create(request):

    stripe.api_key = settings.STRIPE_SECRET_KEY

    #cart = Cart(request)
    user = request.user
    profile = Profile.objects.filter(user=user).last()

    cart = Cart.objects.filter(user=user).last()
    cart_items = Item.objects.filter(cart=cart)
    payment_total_value = int(cart.total * 100)
    flag_required = 0

    if request.method == 'POST':
        form = OrderCreateForm(request.POST)

        # if not form.is_valid():
        #     flag_required = 1
        #     order = Order.objects.get(id=user.id)
        #     return render(request,
        #                   'orders/order/create.html',
        #                   {'order': order,'flag_required': flag_required})
        # elif \
        if form.is_valid():
            order = form.save()
            order_number = Order.objects.first()

            for item in cart_items:
                OrderItem.objects.create(order=order,
                                         product=item.product,
                                         price=item.get_total_price(),  #price total dos elementos da linha
                                         quantity=item.quantity)

                # remover stock após venda
                item.product.stock = item.product.stock - item.quantity
                item.product.save()

                # enviar email para cliente
                send_mail('ENCOMENDA REALIZADA nr ' + str(order_number) + ' na RogueBit Shop',
                          'ENCOMENDA REALIZADA nr ' + str(order_number),
                          'geral@inforzen.pt',
                          ['manuelseromenho@gmail.com']) #[user.email]

                # enviar email para empresa
                company_string_subject = 'ENCOMENDA REALIZADA nr ' + str(order_number) + ' na RogueBit Shop'

                company_string_email = 'ENCOMENDA REALIZADA nr ' \
                                       + str(order_number) + ' pelo/a cliente ' + user.first_name + ' ' \
                                       + user.last_name + ' com a morada de entrega: ' + profile.post_address \
                                       + ' e morada de faturação: ' +  profile.billing_address
                send_mail(company_string_subject, company_string_email,'postmaster@sandbox5e65addc56f44613bc8755aa1da8e3d4.mailgun.org',
                          ['geral@inforzen.pt'])

                token = request.POST.get("stripeToken")

                try:
                    charge = stripe.Charge.create(
                        amount=payment_total_value,
                        currency="eur",
                        source=token,
                        description=company_string_email
                    )
                except stripe.error.CardError as ce:
                    return False, ce

        # clear the cart
        cart.delete()

        user_form = UserCreateForm(
            instance=request.user,
            data=request.POST)
        profile_form = ProfileCreateForm(
            instance=request.user.profile,
            data=request.POST,
            files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()

        return render(request, 'orders/order/created.html', {'order': order})
    else:
        form = OrderCreateForm()
        user_form = UserCreateForm(instance=request.user)
        profile_form = ProfileCreateForm(instance=request.user.profile)

        return render(
            request,
            'orders/order/create.html',
            {
                'cart_items': cart_items,
                'form': form,
                'user_form': user_form,
                'profile_form': profile_form,
                'cart':cart,
                'user':user,
                'pk_stripe': settings.STRIPE_PUBLIC_KEY,
                'payment_total_value': payment_total_value
            }
        )
