from django import forms
from .models import Order
from cart.models import Cart, Item
from shop.models import Profile
from django.contrib.auth.models import User


class OrderCreateForm(forms.ModelForm):

    class Meta:
        model = Order
        fields = ['first_name', 'last_name', 'email', 'post_address', 'billing_address', 'nif', 'payment_method']


class UserCreateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileCreateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('post_address', 'billing_address', 'nif', 'payment_method')

    def __init__(self, *args, **kwargs):
        super(ProfileCreateForm, self).__init__(*args, **kwargs)

        # for key in self.fields:
        self.fields['post_address'].required = True
        self.fields['billing_address'].required = True

